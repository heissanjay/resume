# Resume Repository

This repository contains the LaTeX source code for my professional resume. The resume is created using LaTeX, making it easy to generate a high-quality PDF version whenever needed.

## Table of Contents

- [Resume Preview](#resume-preview)
- [Getting Started](#getting-started)
- [Usage](#usage)

## Resume Preview

You can view the latest version of my resume in the following formats:
- [PDF](https://gitlab.com/sanjay-j/resume/-/blob/main/resume.pdf)

## Getting Started

### Prerequisites

To work with this resume, you need to have the following software/tools installed:

- LaTeX distribution (e.g., TeX Live, MikTeX)
- A LaTeX editor (e.g., TeXShop, TeXworks, VSCode with LaTeX extension)

### Clone the Repository

To get started, clone this repository to your local machine:

```shell
git clone https://gitlab.com/sanjay-j/resume.git
```

### Editing the Resume
Open the LaTeX source file resume.tex in your LaTeX editor. You can make modifications to your resume's content and formatting in this file.

### Usage
Compiling to PDF
To generate the PDF version of your resume from the LaTeX source, you can use the pdflatex command:

```
pdflatex resume.tex
```

This will create a resume.pdf file in the same directory.

Updating HTML Version
If you want to generate an HTML version of your resume, you can use tools like pandoc. For example:

```
pandoc resume.tex -o resume.html
```

